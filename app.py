from flask import Flask, render_template
from flask_cors import CORS 
import datetime

#using Flask and storing into a variable 
app=Flask(__name__)
CORS(app,origins="*")  



# Function to generate random image URL
def get_random_image_url():
    return "https://i.pravatar.cc/300"


# Function to add ordinal indicators to the day part of the date
def ordinal_indicator(day):
    if 10 <= day % 100 <= 20:
        suffix = "th"
    else:
        suffix = {1: "st", 2: "nd", 3: "rd"}.get(day % 10, "th")
    return str(day) + suffix

@app.route("/")
def index():
        today_date = datetime.date(2022, 2, 8)
        formatted_date = ordinal_indicator(today_date.day) + " " + today_date.strftime("%b %Y")
        image_url="https://i.pravatar.cc/300"
        new_image_url = get_random_image_url()
        return render_template('index.html',image_url=new_image_url,today_date=formatted_date, default_image=image_url)
    
 
if __name__ == '__main__':
    app.run(debug=True)